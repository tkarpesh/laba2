program Laba_2;

{$APPTYPE CONSOLE}
uses SysUtils;
             
var
  P, Q: Integer; //coprime numbers;

  Numb: array[1..10000] of integer; //result
  Ost: array[1..10000] of Integer; // array of balance
  MassRes: array[1..10000] of Boolean; // array for determinatoin repearts

  I, K, D: Integer; // counters

  AllResultTrue: Boolean; // for determination the start of repeats
  FirstPN, item, dop: Integer;
  Answ: string;

//Function to find the period of the fraction:
function numberPeriod(P, Q: Integer): string;
begin
  Answ := '0.';
  for I := 1 to (2 * (Q + 1)) do
  begin
    Numb[I] := (P * 10) div Q;
    Ost[I] := (P * 10) mod Q;
    //If there's one residue fraction, which is zero, the franction is final
    if Ost[I] = 0 then
    begin
      for K := 1 to I do
      begin
      Answ := Answ + IntToStr(Numb[K]);
      end;
      Result := Answ;
      Exit;
    end;
    P := Ost[I];
  end;

  FirstPN:= P; //FirstPN - first repetition period
  P := (P * 10) mod Q;
  K := 0;
  while P <> FirstPN do
  begin
    P := (P * 10) mod Q;
    K := K + 1;
  end;

  // Now "K" is the position before fraction's repeting
  I := K;
  D := 0;
  while Ost[i] <> FirstPN do
  begin
    i := i + 1;
    D := D + 1   // D is the length od repeating
  end;
  K := K + 1;
  I := I - 1;
  D := D - 1;
  If D = 0 then
  begin
  for I := 1 to K do
  begin
    if Numb[I] = Numb[K + I + D] then
    begin
      D := D + I ;
      MassRes[I] := True;
    end
  end;
  AllResultTrue := True;
  for I := 1 to K do
  begin
    AllResultTrue := AllResultTrue and MassRes[I];
  end;

  if AllResultTrue then // �eriod comes immediately after the decimal point
  begin
    Answ := '0. (';
    for I :=1 to K do
    begin
      Answ := Answ + intToStr(Numb[I]);
    end;
  end else
  begin
    K := 1;  // �eriod comes immediately after the decimal point
    While Numb[K] <> Numb[K + D] do
    begin
      K := K + 1;
    end;
    K := K - 1;
    for I := 1 to K do
    begin
      Answ := Answ + IntToStr(Numb[I]);
    end;
    Answ := Answ + ' (';
    for I := (K + 1) to (K + D) do
    begin
      Answ := Answ + IntToStr(Numb[I]);
    end;
  end;
  end else
  begin
    for I := 1 to K do
  begin
    if Numb[I] = Numb[K + I] then
    begin
      D := D + I ;
      MassRes[I] := True;
    end
  end;
  AllResultTrue := True;
  for I := 1 to K do
  begin
    AllResultTrue := AllResultTrue and MassRes[I];
  end;

  if AllResultTrue then // �eriod comes immediately after the decimal point
  begin
    Answ := '0. (';
    for I :=1 to K do
    begin
      Answ := Answ + intToStr(Numb[I]);
    end;
  end else
  begin
    K := 1;  // �eriod comes immediately after the decimal point
    While Numb[K] <> Numb[K + I] do
    begin
      K := K + 1;
    end;
    K := K - 1;
    for I := 1 to K do
    begin
      Answ := Answ + IntToStr(Numb[I]);
    end;
    Answ := Answ + ' (';
    for I := (K + 1) to (K * 3) do
    begin
      Answ := Answ + IntToStr(Numb[I]);
    end;
  end;
  end;
  Answ := Answ + ')';
  Result := Answ;
end;

begin
  Writeln('Add P and Q (P < Q) - ');
  Readln(P, Q);
  Answ := numberPeriod(P, Q);
  Writeln(Answ);
  Readln;
end.
